FROM node:20.14.0-alpine@sha256:804aa6a6476a7e2a5df8db28804aa6c1c97904eefb01deed5d6af24bb51d0c81

COPY package.json yarn.lock /

RUN yarn install -g

WORKDIR /code
ENTRYPOINT ["/node_modules/.bin/eslint"]
